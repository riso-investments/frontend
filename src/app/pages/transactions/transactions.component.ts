import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd';
import { TransactionsService } from 'src/app/services/transactions/transactions.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.sass']
})
export class TransactionsComponent implements OnInit {
  public transactions: any;
  public transactionsTableLoading = true;
  public drawerVisible = false;
  public selectedTransaction: any;
  public validateForm: FormGroup;
  public isWriting = false;

  constructor(
    private transactionsService: TransactionsService,
    private formBuilder: FormBuilder,
    private notification: NzNotificationService,
  ) { }

  ngOnInit() {
    this.transactionsService.getTransactions(1).subscribe(data => {
      this.transactions = data;
      this.transactionsTableLoading = false;
    });
    this.validateForm = this.formBuilder.group({
      mnemotechnic: [null, [Validators.required]],
      type: [null, [Validators.required]],
      shares_amount: [null, [Validators.required]],
      share_price: [null, [Validators.required]],
      notes: [null]
    });
  }

  handleNew = () => this.drawerVisible = true;


  handleEdit = (transaction: any) => {
    console.log("Not implement", transaction);
  }

  handleSubmit = () => {
    // tslint:disable-next-line: forin
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }

    if (this.validateForm.invalid) {
      return;
    }

    this.isWriting = true;
    this.transactionsService.create(1, this.validateForm.value).subscribe(
      (data: any) => {
        this.transactions = [
          ...this.transactions,
          data
        ];
        this.isWriting = false;
        this.drawerVisible = false;
        this.notification.create(
          'success',
          'Transactions',
          `The transaction has been created`
        );
      }
    );
  }

  formatterDollar = (value: number) => `$ ${value}`;

  parserDollar = (value: string) => value.replace('$ ', '');

  handledrawerClose = () => {
    this.drawerVisible = false;
  }
}

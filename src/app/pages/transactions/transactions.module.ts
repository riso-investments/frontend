
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzButtonModule, NzDrawerModule, NzFormModule, NzInputModule, NzInputNumberModule, NzNotificationModule, NzTableModule } from 'ng-zorro-antd';

import { TransactionsRoutingModule } from './transactions-routing.module';

import { TransactionsComponent } from './transactions.component';


@NgModule({
  imports: [
    TransactionsRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzTableModule,
    NzButtonModule,
    NzDrawerModule,
    NzInputModule,
    NzFormModule,
    NzInputNumberModule,
    NzButtonModule,
    NzNotificationModule
  ],
  declarations: [TransactionsComponent],
  exports: [TransactionsComponent]
})
export class TransactionsModule { }

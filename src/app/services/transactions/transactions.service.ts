import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';

import { RestService } from 'src/app/services/rest/rest.service';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {
  private prefix = 'transactions';

  constructor(@Inject(RestService) private rest: RestService) { }

  public getTransactions(userId: number): Observable<any> {
    return new Observable<any>(observe => {
      const url = `/users/${userId}/${this.prefix}`;

      this.rest.get(url).subscribe(
        (data: any) => {
          observe.next(data);
          observe.complete();
        }, (error: any) => {
          observe.next(error);
          observe.complete();
        }
      );
    });
  }

  public create(userId: number, transaction: any): Observable<any> {
    return new Observable<any>(observe => {
      this.rest.post(`/users/${userId}/${this.prefix}`, transaction).subscribe((data: any) => {
        observe.next(data);
        observe.complete();
      }, (error: any) => {
        observe.next(error);
        observe.complete();
      });
    });
  }
}
